const chai = require('chai');

const sinon = require('sinon');

chai.use(require('sinon-chai'));

const { expect } = chai;

const sandbox = sinon.createSandbox();

const handlerStub = sandbox.stub();

const handle = (request, response, next) =>
    handlerStub(request, response) || next();

const Continuation = require('../../src/continuation');

const subject = new Continuation();

describe('Continuation', () => {
    afterEach(() => sandbox.reset());

    describe('#follow(request,response,...handlers)', () => {
        it('Should activate the handler once', async () => {
            await subject.follow('request', 'response', handle);
            expect(handlerStub).to.be.calledOnceWith('request','response');
        });
        it('Should activate the handler twice', async () => {
            await subject.follow('request', 'response', handle, handle);
            expect(handlerStub).to.be.calledTwice.and.calledWith('request','response');
        });
        it('Should break the activation chain', async () => {
            handlerStub.returns(true);
            await subject.follow('request', 'response', handle, handle);
            expect(handlerStub).to.be.calledOnce.and.calledWith('request','response');
        });
    });
});
