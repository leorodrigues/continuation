module.exports = class Continuation {
    async follow(request, response, ...handlers) {
        const pending = [ ];
        const iterator = handlers[Symbol.iterator]();
        const nextFn = forward(pending, iterator);
        nextFn();
        while (pending.length > 0) {
            const handler = pending.pop();
            await handler(request, response, nextFn);
        }
    }
};

function forward(pending, iterator) {
    return () => {
        if (pending.length > 0)
            return;
        const next = iterator.next();
        if (next.done)
            return;
        pending.push(next.value);
    };
}
